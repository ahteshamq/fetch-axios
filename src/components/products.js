import React, { Component } from 'react';
import axios from 'axios';
import '../styles/products.css'




class Products extends Component {

    constructor() {
        super();
        this.state = {
            products: [],
            didReceive: false,
            zeroProduct: null,
            isLoaded: false,
            displayLoader: true,
        };

        this.URL = 'https://fakestoreapi.com/products';
    }

    fetchData = (url) => {
        return axios.get(url)
            .then((response) => {
                return response.data
            }).then((data) => {
                this.setState({
                    products: [...data],
                    isLoaded: true,
                    displayLoader: false
                })
            })
            .catch((error) => {
                this.setState({
                    didReceive: true
                })
            })
    }

    componentDidMount = () => {
        this.fetchData(this.URL)
    }

    render() {
        let products = this.state.products;
        return (
            <ul>
                {this.state.displayLoader === true &&
                    <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
                }
                {

                    this.state.isLoaded && products.map((prod) => {
                        return <li className="cart">
                            <div className='img'><img src={prod.image}></img></div>
                            <div className='product'>
                                <h2>{prod.title}</h2>
                                <p>{prod.description}
                                </p>
                                <div className='description'>
                                    <h3>{prod.category}</h3>
                                </div>
                                <div className='action-button'>
                                    <span><i className="fa-solid fa-gift"></i>{prod.rating.rate}</span>
                                    <span>|</span>
                                    <span><i className="fa-solid fa-trash-can"></i>{prod.rating.count}</span>
                                </div>
                            </div>
                            <div className="f-rate">
                                <span><b>&dollar; {prod.price}</b></span>
                            </div>
                        </li>
                    })
                }

                {this.state.didReceive === true &&
                    <div className='error'>
                        <h1>Oops! Products Could Not Be Loaded</h1>
                    </div>
                }
                {console.log(products.length)}
                {products.length === 0 &&
                    <div className='error'>
                        <h1>Oops! Products Could Not Be 0</h1>
                    </div>
                }


            </ul>
        );
    }
}

export default Products;